https://docs.exoplatform.org/public/index.jsp?topic=%2FPLF40%2FPLFRefGuide.Introduction.CMIS.Features.Search.html
```
SELECT * FROM cmis:document WHERE CONTAINS(\'bar\') AND IN_FOLDER('<uid>')
SELECT * FROM cmis:document WHERE cmis:name LIKE '%foo%' AND IN_TREE('<uid>')
SELECT * FROM cmis:folder
SELECT * FROM cmis:xxxxxxxx WHERE CONTAINS ('TAG:testtag')
SELECT * FROM cmis:document WHERE (cmis:lastModificationDate >= TIMESTAMP '2007-01-01T00:00:00.000Z')"
```