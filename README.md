# odoo

## alfodoo
### install
(http://alfodoo.org/guides/short_guide.html)

install cmislib on server
```
pip install 'cmislib>=0.6'
```

download cmis by oca :
https://github.com/OCA/connector-cmis/tree/10.0  
download alfodoo :
https://github.com/acsone/alfodoo  

in `odoo/addons` we should have :
```
cmis/
cmis_alf/
cmis_field/
cmis_web/
cmis_web_alf/
cmis_web_proxy/
cmis_web_proxy_alf/
```

### load cmis_field
one can just load cmis_field from cmd-line : `--load web,web_gantt,cmis_field`

### configure in Settings > CMIS > Backend
Location : `http://alfresco.server:8080/alfresco/api/-default-/public/cmis/versions/1.1/browser/`  
Share Url : `http://alfresco.server:8080/share`  
Alfresco Api Url : `http://alfresco.server/alfresco/s/api`  
initial directory for writing : `/sites/odoo-backend/documentlibrary/testing`

### backend options
strategy in case of duplicate : `Create as "name_(x)"`
where x is an incremented int.
This lets use having multiple employees with the same name.

### add the cmis_folder to an existing model
Extend a module and add the a `cmis_folder` field:
```py
cmis_folder = fields.CmisFolder()
```

### configure in Settings > CMIS > Backend
* Location : `http://alfresco.server:8080/alfresco/api/-default-/public/cmis/versions/1.1/browser/`
* Share Url : `http://alfresco.server:8080/share`
* Alfresco Api Url : `http://alfresco.server/alfresco/s/api`
* initial directory for writing : `/sites/odoo-backend/documentlibrary/<root-folder-name>`

### backend options
strategy in case of duplicate : `Create as "name_(x)"`
where x is an incremented int.
This lets use having multiple employees with the same name.

### add the cmis_folder to an existing model
Extend a module and add the a `cmis_folder` field:
```python
cmis_folder = fields.CmisFolder()
```

### Widget
Simply add a `<field name="cmis_folder"/>` in the extended view.
For example, adding a tab in the `hr.employee` forms :
```xml
<record id="view_employee_form" model="ir.ui.view">
  <field name="name">hr.employee.form (cmis_hr_employee)</field>
  <field name="model">hr.employee</field>
  <!-- extend existing form -->
  <field name="inherit_id" ref="hr.view_employee_form"/>
  <field name="arch" type="xml">
    <notebook position="inside"> <!-- odoo's simplified xpath -->
      <page string="Documents" groups="hr.group_hr_user">
        <field name="cmis_folder" />
      </page>
    </notebook>
  </field>
</record>
```

   
# Alfresco
## enable CORS
in `alfresco-root-dir/tomcat/conf/web`
```xml
<!-- CORS Filter Begin -->
<filter>
    <filter-name>CORS</filter-name>
    <filter-class>org.apache.catalina.filters.CorsFilter</filter-class>
    <init-param>
      <param-name>cors.allowed.origins</param-name>
      <param-value>http://10.0.0.11:8069,https://192.168.120.22</param-value>
    </init-param>
    <init-param>
      <param-name>cors.allowed.methods</param-name>
      <param-value>GET,POST,HEAD,OPTIONS,PUT,DELETE</param-value>
    </init-param>
    <init-param>
      <param-name>cors.allowed.headers</param-name>
      <param-value>origin, authorization, x-file-size, x-file-name, content-type, accept, x-file-type, DNT, x-customheader ,keep-alive ,user-agent ,x-requested-with ,if-modified-since, cache-control,accept-ranges,content-encoding,content-length</param-value>
    </init-param>
    <init-param>
      <param-name>cors.exposed.headers</param-name>
      <param-value>origin, authorization, x-file-size, x-file-name, content-type, accept, x-file-type, DNT, x-customheader ,keep-alive ,user-agent ,x-requested-with ,if-modified-since, cache-control,accept-ranges,content-encoding,content-length</param-value>
    </init-param>
    <init-param>
      <param-name>cors.support.credentials</param-name>
      <param-value>true</param-value>
    </init-param>
    <init-param>
      <param-name>cors.preflight.maxage</param-name>
      <param-value>3600</param-value>
    </init-param>
 </filter>
 <!-- CORS Filter End -->

 <!-- CORS Filter Mappings Begin -->
 <filter-mapping>
    <filter-name>CORS</filter-name>
    <url-pattern>/api/*</url-pattern>
    <url-pattern>/service/*</url-pattern>
    <url-pattern>/s/*</url-pattern>
    <url-pattern>/cmisbrowser/*</url-pattern>
 </filter-mapping>
 ```
