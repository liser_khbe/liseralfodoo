# Sidebar
`require(web.Sidebar)`  

This is the widget that handles the sidebar. Therefore also responsible for the 
print/Action dropdown menu. 

## Attachments
This dropdown menu is rendered by the qweb template called `Sidebar` defined in 
`web/static/src/xml/base.xml`. This template makes the `.btn-group.o_dropdown` for 
each dropdown "menu" (print, action, attachments) and the `.dropdown-menu` itself.

### dropdown-menu
A foreach on `widget.items[section.name]` where `section_name == 'files'`.
Renders a tooltip (info such as name, creation date, modfication date) and the 
text inside the `<li>` itself. This element is a `<a>` tag with a link to the 
downloadable url.

### methods
* `on_attachment_changed`  
    Called when a file has been picked.
    _changes: upload the file to the cmis (we check if a cmis_folder field exists).
* `redraw`  
    This is the method that calls the QWeb.render and passes it the items array.
* `add_items(section_code, items)`  
    First param is the name of the menu ('print', 'files', 'action'),
    2nd is a __list__ of the items to add.
* `on_attachment_loaded`  
    Called in `on_attachment_update`, give a label and url based on name and id.
* `on_attachment_update`  
    Called in the `init` and `on_attachment_delete` updates the content of the dataset (?).

#### File object
inside the first element (tooltip)
* `item.name` : this is the file name
* `item.create_uid[1]` + `item.create_date`
* `item.write_uid[1]` + `item.write_date`  
_note on uids : they are arrays, the first element contains id, the 2nd the name._
* `item.url` contains the download link.
* `item.label` this is the displayed name.