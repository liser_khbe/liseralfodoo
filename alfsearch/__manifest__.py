{
    'name': 'Alfresco search',
    'description': """Module to query Alfresco's cmis repository from a controller
        and a web search""",
    'category': 'Document Management',
    'version': '0.1',
    'author': 'liser',
    'website': 'https://www.liser.lu/',
    'depends': ['alfdocs_urls', 'website', 'web'],
    'data' : [
        'views/alfsearch.xml',
        'views/cmis_backend_view.xml'
    ],
}

    # @http.route("/alfquery", auth='public', type="json")
    # def alfquery(self, query, **kwargs):
    #     return json.dumps({'foo': query})

    #     # SELECT * FROM cmis:document WHERE IN_TREE('cmis_folder_id')