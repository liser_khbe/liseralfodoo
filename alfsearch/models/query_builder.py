from datetime import datetime,timedelta
from dateutil import parser

import logging

class query_builder:
    # todo : synctactic sugar to add an entire list of keywords/simple, etc.
    qs = {
        # todo : keyword checks for title,author too ?
        "keyword": "{1}(CONTAINS('cmis:name:\\'*{0}*\\'') OR CONTAINS('TAG:\\'{0}\\''))",
        "simple" : "{2}(CONTAINS('{0}:\\'{1}\\''))",
        "double" : "{3}(CONTAINS('{0}:\\'{2}\\'') OR CONTAINS('{1}:\\'{2}\\''))",
        "date"   : "({0} {1} TIMESTAMP '{2}')",
        "tree"   : "{1}IN_TREE('{0}')",
        "folder" : "{1}IN_FOLDER('{0}')"
    }
    custom_so = ""

    def __init__(self, fields="*", objecttype="cmis:document"):
        self.fields = fields
        self.objecttype = objecttype
        self.filters = []
        self._logger = logging.getLogger(__name__)

    def add_one_filter(self, filter):
        """Add a custom filter
        Args:
            param1 (str) : a where clause
        """
        self._logger.debug(filter)
        self.filters.append(filter)
        return filter

    def add_filters(self, filters):
        """Add a list of custom filter to the where clause
        Args:
            param1 (list) : a list of filters (str)
        """
        s = self
        for f in filters:
            s = self.add_one_filter(f)
        return self

    def add_tree(self, value, negate=False):
        """Adds a IN_TREE filter.
        Args:
            param1 (str | list) : it doesn't really make sense to have to pass a list,
            except when used with negate.
        """
        if(isinstance(value, list)):
            q = map(lambda x: self.qs["tree"].format(x, "NOT " if(negate) else ""),
                    value)
        else:
            q = [self.qs["tree"].format(value, "NOT " if(negate) else "")]
        return self.add_filters(q)

    def add_folder(self, value, negate=False):
        """Adds a IN_FOLDER filter.

        Args:
            param1 (str | list) : uid or list of uids.
        """
        if(isinstance(value, list)):
            q = map(lambda x: self.qs["folder"].format(x, "NOT " if(negate) else ""),
                    value)
        else:
            q = [self.qs["folder"].format(value, "NOT " if(negate) else "")]
        return self.add_filters(q)

    # todo : move TypeError of keywords/tags inside here
    def add_keyword(self, values, negate=False):
        """See the qs dict for the filter it adds.
        Args:
            param1 (list): list of keywords.
        """
        q = map(lambda x: self.qs["keyword"].format(x, "NOT " if (negate) else ""),
                values)
        return self.add_filters(q)

    def add_simple(self, pairs, negate=False):
        """Adds simple CONTAINS filters.
        Args:
            param1 (list): list of (key, value) pairs. Key is field name.
        """
        q = map(lambda (k,v): self.qs["simple"].format(k, v, "NOT " if(negate) else ""),
                pairs)
        return self.add_filters(q)

    def add_double(self, values, negate=False):
        """Adds filters on two fields with same value.
        i.e. `(CONTAINS(key1:value) OR CONTAINS(key2:value))

        Args:
            param1 (list): list of (key1, key2, value)-triples
        """
        q = map(lambda (k1,k2,v): self.qs["double"].format(k1, k2, v, 
                    "NOT " if(negate) else ""),
                values)

        return self.add_filters(q)

    def add_date(self, values):
        """Adds datetime filters on the key-field.
        e.g. `(key >= TIMESPAN 'value')`.
        When using equality opertors (==, !=) it checks for the day
        (between previous and next day)

        Args:
            param1 (list): list of (key, op, value)-triples
        """
        op_table = {
            "before":"<=",
            "<"     :"<=",
            "<="    :"<=",
            "after" :">=",
            ">"     :">=",
            ">="    :">=",
            "=="    :"",
            "="     :"",
            "is"    :"",
            "not"   :"NOT ",
            "is not":"NOT ",
            "!="    :"NOT ",
            "!"     :"NOT "
        }
        inv = {">=": "<=", "<=":">="}

        def datefmt(num):
            return "{"+str(num)+":%Y-%m-%d}T00:00:00.000Z"

        def qday():
            q = "{0}({1} AND {2})"
            return q.format(
                "{1}",
                self.qs["date"].format("{0}", ">=", datefmt(2)),
                self.qs["date"].format("{0}", "<=", datefmt(3)))

        def date_query(key, op, value):
            try:
                date = parser.parse(value)
            except:
                self._logger.error("could not parse datetime: {0}".format(value))
                # todo : if builder pattern, return self instead
                return ""

            op = op_table.get(op, "")
            if(op in ["<=", ">="]):
                q = self.qs["date"].format(key, op, datefmt(0).format(date))
            else:
                q = qday().format(key, op, date, date + timedelta(days=1))
            return q

        res = map(lambda (x, y, z): date_query(x,y,z),
                  values)

        return self.add_filters(filter(lambda x: x, res))

    def create(self):
        q = "SELECT {} FROM {}".format(self.fields, self.objecttype)\
            if(not self.custom_so) else self.custom_so

        if(self.filters):
            q += " WHERE {0}".format(self.filters[0])

        for f in self.filters[1:]:
            q += " AND {0}".format(f)

        return q
