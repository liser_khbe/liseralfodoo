from . import query_builder

from odoo import api,models,fields
from odoo.exceptions import ValidationError
import logging

class AlfSearch(models.Model):
    _inherit = 'cmis.backend'
    initial_directory_write_uid = fields.Char(
        compute='_compute_init_dir_uid')

    default_query_root = fields.Char(
        "Root folder for queries",
        required=False)
    empty_query = fields.Boolean(
        "Allow empty queries (not recommended)",
        required=True, default=False)

    @api.depends('initial_directory_write')
    def _compute_init_dir_uid(self):
        f = self.get_folder_by_path(
                    self.initial_directory_write,
                    create_if_not_found=False)
        if(f):
            self.initial_directory_write_uid = f.id
        else:
            self.initial_directory_write_uid = ""


    @api.multi
    def default_search(self, words, **kwargs):
        """Wrapper for search_repo(), fetches documents, uses the urls filters/converter
            and uses the query_root_uid. Returns an empty list if the query fails.
        """
        root_uid = self.get_query_root_uid()
        converter = self.fromcmisobject_config(True, False)

        try :
            return self.search_repo(root_uid, "urls", "cmis:document", words, 
                                    converter, **kwargs)

        except Exception as e:
            _logger = logging.getLogger(__name__)
            _logger.error(e)
            return []

    @api.multi
    def search_repo(self, root_folder_uid, selects, objecttype, words, converter, **kwargs):
        """Execute a query on the cmis repo
        
        Args:
            param1 (str) : uid of the root of the query, root folder of repo if empty
            param2 (str) : key of the select_filter dict
            param3 (str) : cmis objecttype for the "from" clause
            param4 (list): list of words contained in filename
            param5 (func): function to apply on results before returning (default: self.fromcmis_object)
            **tags (list): list of tags to search with
            **modified (list): list of (comparison operator, creation date string)-doubles
            **created (list): list of (comparison operator, last modification date string)-doubles

        Todo :
            **tree (bool): True to search in the whole tree(default), False only searches in folder
            **author (str): created

        Returns:
            list of dict with {'label', 'url'}
        """
        (words, tags) = self.sanitize_qinput(words, kwargs.get("tags", []))
        author = kwargs.get("author", False)

        qb = query_builder.query_builder(self.select_filter.get(selects, "*"), objecttype)\
                          .add_tree(root_folder_uid)

        base = qb.create()

        qb = qb.add_keyword(words)\
               .add_simple(map(lambda x: ("TAG", x), tags))\
               .add_date(map(lambda x: ("cmis:creationDate", x[0], x[1]),
                             kwargs.get("created", [])))\
               .add_date(map(lambda x: ("cmis:lastModificationDate", x[0], x[1]),
                             kwargs.get("modified", [])))
        q = qb.create()

        repo = self.get_cmis_client().defaultRepository
        if(not converter):
            converter = self.fromcmisobject_config(True, False)

        _logger = logging.getLogger(__name__)
        _logger.debug(q)

        if(base == q and not self.empty_query):
            _logger.warning("empty queries not allowed")
            return []

        return map(converter, repo.query(q).getResults())

    def get_query_root_uid(self):
        """Returns the uid of the default_query_root path or the
            initial_directory_write_uid if the former is not set.
        """
        uid = self.initial_directory_write_uid
        if(self.default_query_root):
            f = self.get_folder_by_path(
                    self.default_query_root,
                    create_if_not_found=False)
            uid = f.id if(f) else uid
        return uid

    def sanitize_qinput(self, words, tags):
        """Return a pair ([str], [str]) with words and tags sanitized.
        """
        # duck typing was a mistake
        if(not isinstance(tags, list) or not isinstance(words, list)):
            raise ValidationError('tags and words must be a list')

        words = map(self.sanitize_contains, words)
        words = filter(lambda x: x != '', words)
        tags = map(self.sanitize_contains, tags)
        tags = filter(lambda x: x != '', tags)
        return (words, tags)

    def sanitize_str(self, word):
        """Return an escaped form of the input string 

        Args:
            param1 (str) : input to sanitize
        """
        escape = ['%', '"', '\'', '_']
        for c in escape:
            word = word.replace(c, r'\{0}'.format(c))

        return word

    def sanitize_contains(self,tag):
        def clean_start(s):
            return s if(len(s) == 0 or s[0].isalnum()) else clean_start(s[1:])

        tag = clean_start(tag)
        illegal = [':', '~']
        for c in illegal:
            tag = tag.replace(c, '')

        return self.sanitize_str(tag)
