odoo.define('alfsearch.main', function (require) {
"use strict";

    var core = require('web.core');
    var website = require('website.website');
    var ajax = require('web.ajax');
    var QWeb = core.qweb;
    QWeb.add_template("/alfsearch/static/src/xml/template.xml");

    function execute_query(args) {
        console.log(args);
        ajax.jsonRpc("/alfresco/search", "call", args)
            .then(function (data) {
                console.log(data);
                $(".stuff").html(core.qweb.render("alfsearch.sresults_snippet", 
                    {cmisdocs: data.results, msg: data.msg}));
            });
    }

    function query_parse(txt) {
        function date_parse(dt){
            if("<>!".includes(dt[0]))
                return [dt[0], dt.slice(1)]
            return ["", dt]
        }
        // todo : refactor in common method
        var word_list = better_explode(txt);
        var tags = word_list.filter(x => x.startsWith("tag:"))
                            .map(x => x.slice("tag:".length));
        var created = word_list.filter(x => x.startsWith("created:"))
                            .map(x => x.slice("created:".length))
                            .map(date_parse);
        var modified = word_list.filter(x => x.startsWith("modified:"))
                            .map(x => x.slice("modified:".length))
                            .map(date_parse);
        var keywords = word_list.filter(function (x) {
            var props = ["created:", "modified:", "tag:"];
            return props.every(y => !x.startsWith(y));
        });

        return {
            'keywords': keywords,
            'tags': tags,
            'created': created,
            'modified': modified,
        };
    }

    /**
     * Splits string in list of words, with support of quotes (multiple words inside quotes)
     * @param  {string} txt
     * @return {array}     [string] or []
     */
    function better_explode(txt) {
        var r = txt.match(/\\?.|^$/g).reduce((p, c) => {
            if(c === '"') {
                p.quote ^= 1;
            }else if(!p.quote && c === ' '){
                p.a.push('')
            }else {
                p.a[p.a.length-1] += c.replace(/\\(.)/, "$1");
            }
            return p;
        }, {a: ['']}).a
        return r.filter(x => x && x != "\t");
    }

    $(".alfsearch-btn").on("click", function(e) {
        console.log(query_parse);
        console.log(better_explode);
        var txt = $('.alfsearch-input').val();
        execute_query(query_parse(txt));
    });
    $(".alfsearch-input").on("keydown", function(e) {
        if(e.key != "Enter")
            return;
        var txt = e.target.value;
        execute_query(query_parse(txt));
    });
});