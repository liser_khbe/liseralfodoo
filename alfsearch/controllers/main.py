from odoo import http
import logging

class AlfSearch(http.Controller):
    @http.route("/alfresco/search", auth='public', type='json')
    def mysmalltest(self, keywords, **kwargs):
        backend = http.request.env['cmis.backend'].search([], limit=1)
        res = backend.default_search(keywords, **kwargs)

        return {
            'results': res,
            'msg': "" if(res) else "no results"
        }
