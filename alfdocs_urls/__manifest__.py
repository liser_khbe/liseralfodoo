{
    'name': 'Alfresco urls',
    'description': """QWeb template to fetch a link of all the documents inside
                        the cmis_folder (of the alfodoo module)""",
    'category': 'Document Management',
    'version': '0.2',
    'author': 'liser',
    'website': 'https://www.liser.lu/',
    'depends': ['cmis_web_alf', 'website_form'],
    'data' : [
        'views/template.xml',
    ],
}
