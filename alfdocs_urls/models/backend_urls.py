import logging

from odoo import api,models

class CmisBackend(models.Model):
    _inherit = 'cmis.backend'

    select_filter = {
        'all': "*",
        'urls': "cmis:name,cmis:objectId,cmis:objectTypeId",
        'url_filter': "cmis:name,cmis:objectTypeId,cmis:objectTypeId"
    }

    @api.multi
    def alfdocs_urls(self, model, model_ids, **kwargs):
        """Returns a dict containing a list of records (corresponding to the 
            models_ids given) and a list of dict (name,url of cmisdocs related).
            If record, cmisfolder or model are not found it returns an empty list.
            More than one id can be given.

        Args:
            param1 (str): name of the model (e.g. "hr.employee")
            param2 (list): list of the records' ids (e.g. [1] or [1,2])
        """

        # todo : make it a _logger.debug(...)
        _logger = logging.getLogger(__name__)
        _logger.debug(map(lambda k: [k, kwargs[k]], kwargs.keys()))

        tmodel = False
        try:
            # this was used in controller, is it still the same
            tmodel = self.env[model]
            records = tmodel.browse(model_ids)
        except:
            if(not tmodel):
                _logger.error("An error occurred while fetching %s", model)
            else:
                _logger.error("Error occurred on fetching ids %s of %s", model_ids, model)
            records = []

        cmisdocs = []
        for r in records:
            if 'cmis_folder' not in tmodel._fields or not r.cmis_folder:
                _logger.error("no cmis_folder for %s (%s,)", r._name, r.id)
                continue

            cmisdocs += self.getUrls(
                r.cmis_folder,
                ddl=kwargs.get('ddl', True),
                folder=kwargs.get('folder', 0),
                name=kwargs.get('name', True))

        return {'records': records,
                'cmisdocs': cmisdocs}

    @api.multi
    def getUrls(self, cmis_folder_id, **kwargs):
        """Returns a list of urls/fienames of all the documents inside the folder

        Args:
            param1 (str): UID of the cmis folder.

            **ddl (bool): url is a direct download link if True (default), or a
                link to the Alfresco's document-details page if False.
            **folder (int): Specify how to handle subfolders
                0 (default) Get documents recursively through subfolders.
                1 Include the subfolder in the list (url to documentLibrary).
                2 Ignore subfolders.
            **name (bool): use the cmis title if True (default),
                use filename instead if False.

        Returns:
            List of {'label': , 'url': } dict
        """

        # todo : implement a 4th mode for folder : a tree structure for subfolders, probably needs a different qweb template to work though
        url_rule = kwargs.get('ddl', True)
        folder_rule = kwargs.get('folder', 0)
        name_rule = kwargs.get('name', True)

        def readFolder(uid):
            """inner function for recursivity
            Returns all the children from a folder as a list of dicts
            if folder_rule is set as 0 it also traverses the subfolders
            """

            qfilter = self.select_filter.get('urls', "*")
            docs = repo.getFolder(uid).getChildren(filter=qfilter).getResults()
            for d in docs:
                if d.getProperties()['cmis:objectTypeId'] != "cmis:folder"\
                    or folder_rule != 0:
                    continue
                docs += readFolder(d.id)

            return docs

        repo = self.get_cmis_client().defaultRepository
        converter = self.fromcmisobject_config(url_rule, name_rule)

        doclist = readFolder(cmis_folder_id)
        if folder_rule != 1:
            doclist = filter(
                lambda x: x.getProperties()['cmis:objectTypeId'] != "cmis:folder",
                doclist)

        return map(converter, doclist)

    def fromcmisobject_config(self, url_rule, name_rule):
        return lambda obj: self.fromcmisobject(obj, url_rule, name_rule)

    # todo : replace those ugly ternaries with something more readable
    def fromcmisobject(self, cmisobj, url_rule, name_rule):
        cod = cmisobj.getProperties()
        folder = cod['cmis:objectTypeId'] == "cmis:folder"
        return {
            # if folder -> name
            # if rule the rule says so -> name
            # if no 'cm:title' prorty exist -> name
            # if 'cm:title' is empty string -> name
            # else -> use title
            'label' : cmisobj.name if folder or name_rule or 'cm:title' \
                not in cod or not cod['cm:title'] \
                else cod['cm:title'],

            'url': self.get_content_details_url(cmisobj.id) if folder \
                or not url_rule \
                else self.getUrl(cmisobj.id)
        }

    def getUrl(self, objectId):
        """getDirectDownloadUrl of cmisobjet

        Args:
            param1 (str): uid of the object

        Returns:
            (str) url
        """
        base = self.get_cmis_client().defaultRepository.getRootFolderUrl()
        uid = objectId.split(';')[0]
        return base + "?objectId=" + uid

