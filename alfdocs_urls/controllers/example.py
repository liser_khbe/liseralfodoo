from odoo import http,api
from odoo.addons.todo_web.controllers.main import Todo
import logging

class UrlsExample(http.Controller):

    @http.route("/alfresco/urltest", auth='public', website=True)
    def route_test(self, **kwargs):
        backend = http.request.env['cmis.backend'].search([], limit=1)

        return http.request.render(
            "alfdocs_urls.urls",
            backend.alfdocs_urls("hr.employee", [2,12]))

    @http.route("/alfresco/geturls", auth="public", website=True)
    def route_urls(self, model, model_ids, **kwargs):
        ids = model_ids
        ids = str(ids)
        ids = ids.strip('"')
        try:
            ids = map(int, ids.split(','))
        except:
            ids = []
            _logger = logging.getLogger(__name__)
            _logger.error("incorrect model_ids : %s", model_ids)

        model = model.strip('"')
        backend = http.request.env['cmis.backend'].search([], limit=1)

        return http.request.render(
            "alfdocs_urls.urls",
            backend.alfdocs_urls(model, ids, **kwargs))
