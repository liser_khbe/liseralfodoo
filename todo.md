# integrating alfresco in odoo

# TODO
* __Alfresco__ LDAP authentication on alfresco server
* __QWeb__ ~~links to documents on a static page~~
* __Search__ ~~Make a controller to search docs in the api of alfresco~~ `alfsearch`
* __Search__ ~~integrate it somewhere else~~ : ajax request
* __Search__ ~~document and comment~~, make an example
* __Search__ *(optional)* make a domain parser to build a query à la odoo search filters

## ON HOLD
* __Sidebar__ ~~Add documents from the sidebar~~
* __Sidebar__ ~~Display in the sidebar~~
* __Sidebar__ hooks for automatic load
* __Sidebar__ Error handling
* __Sidebar__ refactoring : the idea of using `cmis_folder_root_id` just doesn't work, find something else !
* __Sidebar__ pretify More button

* __Fix__ `alfodoo_attach` On some views the cmisdocs are displayed like 2-3 times in the sidebar, also displayed for records that shouldn't have any : due to fetching from the attribute of widget instance, which is not reset by entering a different view/page !!!
