# Cmis_folder field
Just a field containing the uid of the folder on the alfresco server.

## Linking _existing_ alfresco folder to record
It is possible (for various reasons) that a folder already exists on the Alfresco server
and that one would use that one for a record instead of creating a new one.
All you have to do is fetch the uid of that alfresco folder and put it in the cmis_folder field
of the record in odoo's DB.

example in the odoo shell :
```
r = self.env['hr.employee'].search([('name', '=', 'toto')])
r.cmis_folder = "a1b2c3d4-1526-7384-9516-a7b8c9deeffa"	# alfresco's folder unique id
self.env.cr.commit()
```
