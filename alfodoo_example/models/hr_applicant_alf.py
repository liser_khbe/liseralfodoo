# -*- coding: utf-8 -*-
from odoo import models, api
from odoo.addons.cmis_field import fields
import logging

class HrApplicantAlf(models.Model):
	_inherit = 'hr.applicant'

	cmis_folder = fields.CmisFolder()

	@api.multi
	def action_get_attachment_tree_view(self):
		view_id = self.env.ref('test_liser_employee_alfresco_ext.view_form_alfodoo_docs').id
		context = self._context.copy()
		return {
			'type': 'ir.actions.act_window',
			'name': 'Alfresco hosted docs',
			'view_type': 'form',
			'view_mode': 'form',
			'views': [(view_id, 'form')],
			'res_model': 'hr.applicant',
			'view_id': view_id,
			'res_id': self.id,
			'target': 'current',
			'context': context,
		}

		
