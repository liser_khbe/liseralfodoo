# -*- coding: utf-8 -*-
from odoo import models
from odoo.addons.cmis_field import fields

class HrEmployeeAlf(models.Model):
	_inherit = 'hr.employee'

	cmis_folder = fields.CmisFolder()
