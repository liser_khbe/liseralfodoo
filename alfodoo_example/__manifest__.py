# -*- coding: utf-8 -*-
{
    'name': "Alfodoo usage example",
    'author': "LISER",
    'summary': """Add documents page for alfresco to the employees model""",
    'category': 'Employees',
    'description': """
	Extends the hr.employee hr.applicant model with the cmis_folder attribute from the alfodoo module.
    """,
    'version': '0.01',
    'depends': ['hr', 'hr_recruitment', 'cmis_alf' ],
    'auto_install': False,
    'data': [
        'views/hr_employee_alf.xml',
        'views/hr_applicant_alf.xml'
    ],
}
