# Using alfodoo

## the model
Whether you're creating a new model or extending an existing one you need to add the following field :
```
cmis_folder = fields.CmisFolder()
```

## the records
If you want to add a cmis_folder for a particular record you have two options. If you just want to create a new one you should do it through the module (alfodoo widget, controller, function calls, whatever); Or if the folder already exists on the server and you want to link it to a particular record you can just add that one.

To add an existing folder to a record, you should get the uid (documentLibrary id, ObjectId, cmis uid) of the folder and assign it to the cmis_folder field either directly in the postgresSQL database or through the `odoo shell` utility.
e.g. 
```
r = self.env['some.model'].search([('id', '=', 1)])
r.cmis_folder = "a1b2c3d4-1526-7384-9516-a7b8c9deeffa"
self.env.cr.commit()
```
**/!\ important note: this can lead to unwanted behavior with the alfsearch module : the manually assigned folder might not be in the same tree as the default root folder for search queries**

## the views
Either extend an existing view by adding the 
```
<field name="cmis_folder" />
```

Or overload an action by creating a new view (or extending an existing one) and return that one instead of the old one.
