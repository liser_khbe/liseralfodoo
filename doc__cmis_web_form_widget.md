# FieldCmisFolder
## overriding
```js
odoo.define('mymodule.mywidget', function(require){
    "use strict";
    var cmisfolder = require('cmis_web.from_widgets');

    cmisfolder.FieldCmisFolder.include({
        method_i_wanna_override: function() {
            this._super.apply(this, arguments);
            var self = this;
            /* .. */
        },
    });
});
```
## variables
* `$datatable` / `datatable`  
    see datatable.
* `$breadcrum`
    this is the place where the "path" inside the root cmis folder is displayed.
* `cmis_session`
    see below.

## methods
* `datatable_query_cmis_data` : function(data, callback, settings)  
    this is the method called by datable to obtain the required data. 
* `render_folder_actions`  
    method to display buttons upper-left.
* `register_root_content_events`  
    Called when a root folder is initialized, it sets the click listeners on the 
    buttons based on class name in `this.$el.find()`.
* `display_row_details`  
    Displays the details of a document using `qweb` templates 
    defined in `/static/src/xml`.
* `on_click_create_root`  
    Creates a node for the current model in the DMS.
    This uses the rpc : `/web/cmis/field/create_value'`

## datatable
*note: apparently, datables is a plugin for bootstrap. Idk if it's the same but it does
seem very related: datatables.net*
in the CmisFieldFolder there's a place where (I think) all the data is saved : `datatable`
### functions
* `data`  
    returns an object (wrapping an array) with all the data currently in the displayed table. To get the Object wrapper of the first row do `this.datatable.data()[0]`
* `row`  
    returns the obeject wrapper of a row. If not argument is passed : returns the first row. It has a data() method too. So to get the data on the third row : `this.datatable.row(4).data()`
* `rows`  
    rows basically contains an array of the indexes of all  the displayed rows.
    `datatable.rows().data()` is the same as `datatable.data()` I think.
    rows adds a couple methods that could be useful such as `ids`, `state`, `nodes`, `every`.
* `table`, `tables`  
    I do not know what they do, but they also have a data() method. 

### objects
* `colReorder`
* `keys`
* `selector`
* `ajax`

### arrays
* `context` probably something to do with JS itself or one of the libraries. It includes a bunch of information like language, browser options, etc.

# cmis_session
This is a field in the FieldCmisFolder.
It only has two objects (the rest are functions) : `repositories` and `defaultRepository`.

## `getChildren(folder_id, options)`
This is the method used to get stuff form the alfresco server.
In short it returns the items of the folder_id passed in parameter.
option is an object, and is mostly what controls the paging.

In `datatable_query_cmis_data`, 3 options are added to the Defaults :
* `maxItems` : the number of items that need to be displayed (this means we don't fetch more than was is displayed in the table.)
* `skipCount` : (page -1) * maxItems.
* `orderBy` : defined by `settings.aaSorting`. 
the `DEFAULT_CMIS_OPTIONS` are :  
```
{
    includeAllowableActions : true,
    renditionFilter : 'application/pdf'
}
```
_example_ :  
```
var opts = { includeAllowableActions : true,
            renditionFilter : 'application/pdf'
            maxItems: 10,
            skipCount: 0,
            orderBy: "cmis:baseTypeId ASC,cmis:name ASC",
           };

cmis_session.getChildren("uid", opts)
            .ok(function(result){ 
                sidebar.add_items("files", _.map(result.objects, 
                                                sidebar.cmis_to_item);
            });
```

## dates
the fields `cmis:lastModificationDate` and `cmis:creationDate` are unix timestamps
(integers), while the QWeb rendered from the sidebar expects a string.


# CmisCreateDocumentDialog
extends the `web.Dialog` widget for the creation of a folder.


