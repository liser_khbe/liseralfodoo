{
    'name': 'Alfesco extension for attachment',
    'description': "Extends the attachment drop-down menu\
     to support Alfresco's integration",
    'category': 'Document Management',
    'version' : '0.2',
    'author': 'LISER',
    'website': 'https://www.liser.lu/',
    'depends': ['web', 'cmis_web_alf'],
    'data': [
        "views/attach.xml"
    ],
    'qweb' : [
        "static/src/xml/*.xml",
    ]
}
