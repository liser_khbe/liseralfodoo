var thissidebar;
/* utils : */
String.prototype.replaceAt=function(index, replacement) {
    return this.substr(0, index)
            + replacement
            + this.substr(index + replacement.length);
}
function dbg(msg) {
    return '[DEBUG] '+ new Date().toLocaleTimeString().slice(0,-3)+ ' - ' + msg;
}

odoo.define('alfodoo_attach.form_widgets', function(require){
    "use strict";

    var core = require('web.core');
    var alfwidget = require('cmis_web.form_widgets');
    var framework = require('web.framework');
    var sidebar = require('web.Sidebar');
    var Model = require('web.Model');

    /* TODO : make this a field of the cmis_backend model and instead of 
     *        hardcoding it, let the user/admin configure that in the Settings
     */
    var blob_fallback = false;


    /* defined in : alfodoo cmis_web/static/src/js/form_widgets.js
     * extended/overloaded in : cmis_web_alf, probably cmis_web_proxy* too
     */
    alfwidget.FieldCmisFolder.include({
        /**
         * The original method in alfodoo only sets the click listener on 
         *   `this.$el.find('.root-content-action-refresh')` 
         * to include the Add from attachments' sidebar we need to extend it.
         * Method called when a root folder is initialized 
         */
        register_root_content_events: function(){
            var self = this;
            this._super.apply(this, arguments);

            $('ul.dropdown-menu li.root-content-action-new-doc').on('click', function(e){
                var dialog = new alfwidget.CmisCreateDocumentDialog(
                                self,
                                self.dislayed_folder_cmisobject); 
                dialog.open();
            });
        },
        /** [TESTING]
         * By default the table is not rendered if the control is displayed in an
         * inactive tab. Problem : not only the table isn't rendered by all the other 
         * stuff related to the cmis_session, etc. isn't initialized either.
         * So for the moment we'll also render the table when accessing the dropdown
         * ideally I'd just like to fetch the data without rendering the table itself
         * if that's possible.
         */
        add_tab_listener : function() {
            this._super.apply(this, arguments);
            thissidebar.do_attachment_update_wrap();
        },
        /**
         * New object wrapper for cmis, basically converts the object fetched from the 
         * cmis into one compatible as a file item for the sidebar widget.
         * @param {Object} obj The object fetched from the cmis_session representing a
         * document.
         * @return {Object} Returns a new object apropriate for the sidebar widget.
         */
        to_sidebar_item : function(obj) {
            function _get(field_name) {
                return obj.object.succinctProperties['cmis:'+field_name];
            }
            function datefmt(dt) {
                var d = new Date(dt);
                return d.toISOString()
                        .slice(0, -5)
                        .replaceAt(10, " ");
            }

            var self = this;
            return {
                create_date: datefmt(_get('creationDate')),
                create_uid: [6, _get('createdBy')],
                label: _get('name'),
                name : _get('name'),
                type: _get('mimetype'),
                folder: _get('objectTypeId') === "cmis:folder",
                write_uid: [1, _get('lastModifiedBy')],
                write_date: datefmt(_get('lastModificationDate')),
                url: self.cmis_session.getContentStreamURL(_get('objectId'), 'attachment'),
                objectId: _get('objectId'),
            }
        },

        /**
         * add a click listener on the on the refresh button from alfodoo's widget
         * TODO : delete this, the logic has been moved to the sidebar widget
         */ 
        register_root_content_events : function () {
            this._super.apply(this, arguments);
            var self = this;
            this.$el.find('.root-content-action-refresh')
                    .on('click', function(e) {
                        thissidebar.do_attachment_update_wrap();
                        thissidebar.cmis_folder_root_id = self.root_folder_id;
                    });
            console.log(dbg('dbgthis set'))
            /*DEBUG*/dbgthis = this;
        },
        /* debugging */
        display_row_details: function(row) {
            this._super.apply(this, arguments);
            console.log(dbg("display_row_details"));
        },
        /* debugging */
        datatable_query_cmis_data: function(data, callback, settings) {
            this._super.apply(this, arguments);
            console.log(dbg('datatable_query_cmis_data() called.'))
        },
    });
  
    /* from the web module : web/static/js/widgets/sidebar.js */
    sidebar.include({
        /**
         * This is a copy of the CmisCreateFolderDialog.on_click_create() from 
         * alfodoo/cmis_web module by Laurent Mignon
         *
         * method called after a file has been selected (from the Add... Button)
         * this uploads the files to the alfresco backend instead of in the odoo filestore
         * except if no 'cmis_folder' field exists.
         *
         * TODO : make this behavior configurable (fallback to blob upload ?)
         * @param {Event} e jquery's eventObject
         */
        on_attachment_changed: function (e) {
            var parent = this.getParent();
            var cmis_folder_field = parent.fields['cmis_folder'];

            /* get the root folder : */
            if (parent.fields['cmis_folder']) {
                var self = this;
                var input = this.$el.find("input[type='file']")[0];
                var numFiles = input.files ? input.files.length : 1;
                var processedFiles = [];
                if (numFiles > 0) {
                    framework.blockUI();
                }

                var cmis_session = cmis_folder_field.cmis_session;
                _.each(input.files, function(file, index, list){
                    cmis_session
                    .createDocument(cmis_folder_field.root_folder_id,
                        file,
                        {'cmis:name': file.name},
                        file.mimeType)
                    .ok(function(data) {
                        processedFiles.push(data);
                        if(processedFiles.length == numFiles){
                            framework.unblockUI();
                        }
                    })
                });
            } else if(blob_fallback) {
                this._super.apply(this, arguments);
            } else {
                /* todo : raise a proper error instead*/
                this.do_warn(core._t('Error uploading'), 
                    "This record does not have a cmis folder and/or blob uploads are not authorized");
            }

            console.log(dbg("on_attachment_changed called"));
        },
        /**
         * Called from do_attachement_update()
         * Responsible for setting the label and the urls of binary typed files.
         * [WARNING] this one takes an `s` : on_attachment[S]_loaded
         *
         * @param {List} attachments list of the attachments in items['files']
         */
        on_attachments_loaded: function(attachments) {
            console.log(this);
            if(this.getParent())
                var cmis_folder = this.getParent().fields['cmis_folder'];
            var self = this;

            var i= attachments.filter(a => a.objectId).length;
            _.each(attachments, function(a) {
                a.label = a.name;

                if(a.type && a.type === "binary") {
                    a.url = "/web/content/" + a.id + "?download=true";
                } else if(a.objectId && cmis_folder) {
                    /* todo : this can be better done as an attribute of widget */
                    /* e.g. : this.cmis_folder_root_id */
                    new Model('cmis.backend')
                    .call("get_content_details_url", [
                        [cmis_folder.backend.id],
                        a.objectId,
                        cmis_folder.view.dataset.get_context()
                    ])
                    .then(function (url) {
                        if(a.folder)
                            a.url = url;
                        a.o_alfresco = url;

                        /* call redraw() on the last iteration */
                        if(! --i)
                            self.redraw();
                    });
                }
            });

            this.items.files = attachments;
            this.redraw();
            console.log(dbg("on_attachments_loaded called"));
        },
        /**
         * This is the method responsible for populating the items list with the 
         * attachments of the record from the DB by doing a datasearch
         * [WARNING] there's an unfixed typo in the name : do_attach[E]ment_update
         *           they'll probably never fix it for compability reasons I guess.
         *
         * We overload it by first calling the base method (_super), which will fill
         * the items['files'] list with the attachments of the DB. We then append
         * the documents located in the alfesco's root folder of this record.
         */
        do_attachement_update: function(dataset, model_id, args) {
            this._super.apply(this, arguments);
            var self = this;
            console.log(dbg("do_attachments_update() called"));

            /* IMPORTANT TODO : replace dbgthis 
             * this just doesn't work as it continues to exist when browsing other 
             * modules/records
             */
            if(!dbgthis) 
                return;

            let count = 10 - this.items['files'].length;
            let cf = dbgthis;
            let options = {
                includeAllowableActions : true,
                renditionFilter : 'application/pdf',
                maxItems: count > 2 ? count : 3, /* max 10 items, at least 3 */
                skipCount: 0,
                orderBy: "cmis:baseTypeId ASC,cmis:name ASC",
            };
            console.log(dbg('options in do_attachement_update'))
            console.log(options);
            cf.cmis_session.getChildren(cf.root_folder_id, options)
                .ok( function(result) {
                    console.log(dbg("getChildren"));
                    console.log(result);
                    var cmis_items = _.map(result.objects, cf.to_sidebar_item, cf);
                    self.items['files'].push.apply(self.items['files'], cmis_items);
                    self.on_attachments_loaded(self.items['files']);
                });
        },
        /**
         * Small wrapper to call do_attachement_update easier from outside.
         */
        do_attachment_update_wrap: function() {
            console.log(dbg("print this in do_attachment_update_wrap()"));
            this.do_attachement_update(this.dataset, this.model_id);
        },
        /**
         * Function used to redraw the dropdown menu : it does so by calling the
         * the Qweb Renderer (sidebar template). Also adds the listeners on the 
         * .o_sidebar_add_attachment and .o_sidebar_delete_attachment
         *
         * We need to overload this so we can add our new listener for cmis documents
         */
        redraw: function() {
            this._super.apply(this);
            this.$('.o_sidebar_alfresco_attachment').click(this.on_open_alfresco);
            this.$('.o_sidebar_alf_root_attachment').click(this.on_open_root_folder);
            /*DEBUG*/thissidebar = this;
            console.log(dbg("thissidebar set in redraw()"));
        },
        /**
         * Opens the caller item in alfresco.
         *
         * @param {Event} e Jquery's eventObject
         */
        on_open_alfresco: function(e) {
            e.preventDefault();
            e.stopPropagation();
            var $e = $(e.currentTarget);
            var url = $e.attr('target-url');
            if(url) {
                window.open(url, '_blank');
            }
            console.log(dbg('on_open_alfresco() called'));
        },
        /**
         * Opens the root folder in alfresco of the current record.
         *
         * @param  {Event} e Jquery's eventObject
         */
        on_open_root_folder: function(e) {
            /* TODO IMPORTANT : pass through an action, href or similar in qweb */
            /* I don't like having to depend on the cmis_folder reference */
            e.preventDefault();
            e.stopPropagation();
            console.log(dbgthis.backend.id); 
            console.log(this.cmis_folder_root_id);
            console .log(this.dataset.get_context());
            if(!this.cmis_folder_root_id)
                return;
            
            new Model("cmis.backend")
            .call("get_content_details_url", 
                [[dbgthis.backend.id],
                 this.cmis_folder_root_id,
                 this.dataset.get_context()
                ])
            .then(function (url) {
                window.open(url, '_blank');
                console.log(dbg('window opened: '+url));
            });
            console.log(dbg('on_open_root_folder called'));
        },
    });

});
